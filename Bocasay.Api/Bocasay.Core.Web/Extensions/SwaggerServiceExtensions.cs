﻿using System.Linq;
using KassomPoc.Core.Web.Swashbuckle;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Bocasay.Core.Web.Extensions
{
    public static class SwaggerServiceExtensions
    {
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", new OpenApiInfo {Title = "Bocasay API v1.0", Version = "v1.0"});

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                // Swagger 2.+ support
                var security = new OpenApiSecurityRequirement();
                security.Add(new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    },
                    Scheme = "oauth2",
                    Name = "Bearer",
                    In = ParameterLocation.Header
                }, new string[] { });

                c.AddSecurityRequirement(security);

                c.ResolveConflictingActions(ApiDescriptionConflictResolver.Resolve);

                // UseFullTypeNameInSchemaIds replacement for .NET Core
                c.CustomSchemaIds(p => p.FullName);
                c.MapType(typeof(IFormFile), () => new OpenApiSchema
                    {Type = "file", Format = "binary"});
            });
            services.AddSwaggerGenNewtonsoftSupport();

            return services;
        }

        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "API v1.0");
                c.DocumentTitle = "API Documentation";
                c.DocExpansion(DocExpansion.None);
            });

            return app;
        }
    }
}
