﻿using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace Bocasay.Core.Web.Extensions
{
    public static class RegisterServices
    {
        public static IServiceCollection AutoInjectServices(this IServiceCollection services)
        {
            services
                .WithScopedLifetime<ICore>()
                .WithScopedLifetime<ICoreWeb>()
                ;

            services.AddMediatR(typeof(ICore));

            services.AddHttpContextAccessor();

            services.AddHttpClient();

            return services;
        }

        public static IServiceCollection RegisterConfiguration(this IServiceCollection services, IConfiguration Configuration)
        {
            // configure strongly typed settings objects
            return services;
        }

        public static IServiceCollection RegisterLogger(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddLogging(loggingBuilder =>
                {
                    // configure Logging with NLog
                    loggingBuilder.ClearProviders();
                    loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                    loggingBuilder.AddNLog(configuration);
                });

            return services;
        }
    }
}
