﻿using Newtonsoft.Json;

namespace Bocasay.Core.Common.Exceptions
{
    public class ErrorItem
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
