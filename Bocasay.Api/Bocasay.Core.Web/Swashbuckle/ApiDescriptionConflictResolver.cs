﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace KassomPoc.Core.Web.Swashbuckle
{
    public static class ApiDescriptionConflictResolver
    {
        public static ApiDescription Resolve(IEnumerable<ApiDescription> descriptions)
        {
            var parameters = descriptions
                .SelectMany(desc => desc.ParameterDescriptions)
                .GroupBy(x => x, (x, xs) => new {IsOptional = xs.Count() == 1, Parameter = x}, ApiParameterDescriptionEqualityComparer.Instance)
                .ToList();
            var description = descriptions.First();
            description.ParameterDescriptions.Clear();
            parameters.ForEach(p =>
            {
                if (p.Parameter.RouteInfo != null)
                    p.Parameter.RouteInfo.IsOptional = p.IsOptional;
                description.ParameterDescriptions.Add(p.Parameter);
            });
            return description;
        }
    }
}
