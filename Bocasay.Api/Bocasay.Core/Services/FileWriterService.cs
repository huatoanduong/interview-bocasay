﻿using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Bocasay.Core.Services
{
    public interface IFileWriterService
    {
        Task WriteFileAsync(object obj, CancellationToken cancellationToken);
    }

    public class FileWriterService : IFileWriterService
    {
        public async Task WriteFileAsync(object obj, CancellationToken cancellationToken)
        {
            var objValue = JsonConvert.SerializeObject(obj);

            using (StreamWriter writetext = new StreamWriter("write.txt"))
            {
                await writetext.WriteAsync(objValue);
            }
        }
    }
}
