﻿using System.Linq;
using System.Reflection;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace KassomPoc.Core.Web.Swashbuckle
{
    public sealed class SwaggerExcludeFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            if (schema.Properties.Count == 0)
                return;

            var excludedProperties = context
                .Type
                .GetProperties()
                .Where(pi => pi.GetCustomAttribute<SwashbuckleIgnoreAttribute>() != null)
                .Select(m => m.GetCustomAttribute<JsonPropertyAttribute>()?.PropertyName ?? ToCamelCase(m.Name))
                .ToList();

            foreach (var excludedName in excludedProperties)
            {
                if (schema.Properties.ContainsKey(excludedName))
                    schema.Properties.Remove(excludedName);
            }
        }

        private static string ToCamelCase(string value)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return char.ToLowerInvariant(value[0]) + value.Substring(1);
        }
    }
}
