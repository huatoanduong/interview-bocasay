﻿using MediatR;
using Microsoft.AspNetCore.Authorization;

namespace Bocasay.Core.Web.Base
{
    [Authorize]
    public abstract class BaseController : BasePublicController
    {
        protected BaseController(
            IMediator mediator
        ) : base(mediator)
        {
        }
    }
}
