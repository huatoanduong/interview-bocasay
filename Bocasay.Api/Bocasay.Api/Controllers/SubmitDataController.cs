﻿using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Bocasay.Core.SubmitData.Models;
using Bocasay.Core.SubmitData.Requests;
using Bocasay.Core.Web.Base;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Bocasay.Api.Controllers
{
    [ApiController]
    [Route("submitdata")]
    public class SubmitDataController : BasePublicController
    {
        public SubmitDataController(IMediator mediator) : base(mediator)
        {
        }


        [HttpPut("")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UploadData([FromBody]SubmitDataDTO model, CancellationToken cancellationToken)
        {
            var response = await _mediator.Send(new SubmitData.Request
            {
                Model = model
            }, cancellationToken);

            return NullSafeOk(response);
        }
    }
}
