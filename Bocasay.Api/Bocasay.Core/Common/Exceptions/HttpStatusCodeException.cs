﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;

namespace Bocasay.Core.Common.Exceptions
{
    [Serializable]
    public class HttpStatusCodeException : Exception
    {
        public List<ErrorItem> Errors { get; set; }

        public virtual HttpStatusCode HttpStatusCode { get; }

        public HttpStatusCodeException()
        {
        }

        public HttpStatusCodeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected HttpStatusCodeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public HttpStatusCodeException(string message) : base(message)
        {
        }

        public HttpStatusCodeException(ErrorItem error)
        {
            Errors = new List<ErrorItem> {error};
        }

        public HttpStatusCodeException(List<ErrorItem> errors) : base(errors?.FirstOrDefault()?.Message)
        {
            Errors = errors;
        }
    }
}
