﻿using System;
using System.Collections.Generic;
using Bocasay.Core.Common.Exceptions;
using KassomPoc.Core.Web.Swashbuckle;
using Newtonsoft.Json;

namespace Bocasay.Core.Web.Exceptions
{
    public class ClientApiError
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("innerMessage")]
        public string InnerMessage { get; set; }

        [JsonProperty("errors")]
        public List<ErrorItem> Errors { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("errorTime")]
        public DateTime? ErrorTime { get; set; }

        [SwashbuckleIgnore]
        [JsonProperty("stackTrace", NullValueHandling = NullValueHandling.Ignore)]
        public string StackTrace { get; set; }
    }
}
