﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bocasay.Core.SubmitData.Models
{
    public class SubmitDataDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
