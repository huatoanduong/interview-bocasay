﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace KassomPoc.Core.Web.Swashbuckle
{
    public static class CustomSchemaIdCreator
    {
        private static HashSet<string> _usedSchemaId = new HashSet<string>
            {""};

        private static Dictionary<string, string> _fullNameToSchemaId = new Dictionary<string, string>();
        private static HashSet<string> _ignoreNames = new HashSet<string> {"", "ViewModels", "Models", "Model", "Views", "View", "Command", "Query", "Csv", "Commands", "Queries", "Api", "Core", "FusionApi", "LegalDocument", "LegalDocument"};

        public static string Create(Type type)
        {
            if (_fullNameToSchemaId.TryGetValue(type.FullName, out var schemaId))
            {
                return schemaId;
            }

            var cleanTypeName = GetCleanName(type);
            var typeNameSpaces = GetNamespaceList(type);
            var newUniqueSchemaId = GenerateUniqueSchemaId(cleanTypeName, typeNameSpaces);

            _fullNameToSchemaId.Add(type.FullName, newUniqueSchemaId);
            return newUniqueSchemaId;
        }

        private static string GetCleanName(Type type)
        {
            var name = type.Name;

            if (type.IsGenericType)
            {
                var firstGenericArgType = type.GetGenericArguments()?.FirstOrDefault()?.Name ?? string.Empty;
                name = name.Substring(0, name.IndexOf('`')) + firstGenericArgType;
            }

            var cleanName = name
                .RemoveFromEnd("ViewModel")
                .RemoveFromEnd("View")
                .RemoveFromEnd("Model");

            if (cleanName.Length == 0)
                cleanName = name;

            return cleanName;
        }

        private static List<string> GetNamespaceList(Type type)
        {
            string modifiedNamespacePath;

            if (type.IsGenericType)
                modifiedNamespacePath = type.Namespace ?? type.FullName.Replace("+", ".").RemoveFromEnd($".{type.Name}");
            else
                modifiedNamespacePath = type.FullName.Replace("+", ".").RemoveFromEnd($".{type.Name}");

            return modifiedNamespacePath.Split(".")?.Reverse()?.ToList() ?? new List<string>();
        }

        private static string GenerateUniqueSchemaId(string name, List<string> namespaces)
        {
            if (name == null) throw new ArgumentNullException("name");
            if (namespaces == null) throw new ArgumentNullException("namespaces");

            if (!_usedSchemaId.Contains(name) && !_ignoreNames.Contains(name))
            {
                _usedSchemaId.Add(name);
                return name;
            }

            //try name spaces
            var uniqueNameFromNamespaces = GenerateUniqueNameFromNamespaces(name, namespaces);
            if (uniqueNameFromNamespaces != string.Empty && !_usedSchemaId.Contains(uniqueNameFromNamespaces))
            {
                _usedSchemaId.Add(uniqueNameFromNamespaces);
                return uniqueNameFromNamespaces;
            }

            //no namespaces left, start using numbers
            return GenerateUniqueNameByNumber(name);
        }

        private static string GenerateUniqueNameFromNamespaces(string name, List<string> namespaces)
        {
            if (namespaces == null) throw new ArgumentNullException("namespaces");
            if (namespaces.Count == 0) return string.Empty;

            var nextNSName = string.Empty;
            while (_ignoreNames.Contains(nextNSName) && namespaces.Count > 0)
            {
                nextNSName = namespaces[0];
                namespaces.RemoveAt(0);
            }

            if (_ignoreNames.Contains(nextNSName)) return string.Empty;

            var newName = nextNSName + name;
            if (!_usedSchemaId.Contains(newName)) return newName;

            return GenerateUniqueNameFromNamespaces(name, namespaces);
        }

        private static string GenerateUniqueNameByNumber(string name)
        {
            if (name == null) throw new ArgumentNullException("name");

            if (!_usedSchemaId.Contains(name))
            {
                _usedSchemaId.Add(name);
                return name;
            }

            var lastChar = name[name.Length - 1].ToString();
            var count = 1;
            int.TryParse(lastChar, out count);
            count++;
            var newName = name.Remove(name.Length - 1) + count;


            return GenerateUniqueNameByNumber(newName);
        }

        private static string RemoveFromEnd(this string s, string suffix)
        {
            if (s.EndsWith(suffix))
            {
                return s.Substring(0, s.Length - suffix.Length);
            }

            return s;
        }
    }
}
