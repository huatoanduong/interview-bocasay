﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bocasay.Core.Services;
using Bocasay.Core.SubmitData.Models;
using MediatR;

namespace Bocasay.Core.SubmitData.Requests
{
    public class SubmitData
    {
        public class Request : IRequest<bool>
        {
            public SubmitDataDTO Model { get; set; }
        }

        public class Handler : IRequestHandler<Request, bool>
        {
            private readonly IFileWriterService _fileWriterService;

            public Handler(
                IFileWriterService fileWriterService
            )
            {
                _fileWriterService = fileWriterService;
            }

            public async Task<bool> Handle(Request request, CancellationToken cancellationToken)
            {
                await _fileWriterService.WriteFileAsync(request.Model, cancellationToken);

                return true;
            }
        }
    }
}
