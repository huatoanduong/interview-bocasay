﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Bocasay.Core.Common.Exceptions;
using Bocasay.Core.Web.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Bocasay.Core.Web.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;

        public ErrorHandlingMiddleware(RequestDelegate next, IWebHostEnvironment hostingEnvironment, ILogger<ErrorHandlingMiddleware> logger)
        {
            _next = next;
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                await HandleExceptionAsync(context, ex, _hostingEnvironment, false);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception, IWebHostEnvironment hostingEnvironment, bool htmlEncode)
        {
            // set code
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected

            // set message & status code
            var message = "An error occurred.";
            List<ErrorItem> errors = null;

            string stackTrace = null;
            if (hostingEnvironment.IsDevelopment())
            {
                stackTrace = exception.StackTrace;
            }

            if (exception is HttpStatusCodeException statusCodeException)
            {
                code = statusCodeException.HttpStatusCode;
                message = statusCodeException.Message;
                errors = statusCodeException.Errors;
            }

            if (errors != null && htmlEncode)
                errors = HtmlEncode(errors);

            var result = JsonConvert.SerializeObject(
                new ClientApiError
                {
                    Error = "An error occurred.",
                    ErrorTime = DateTime.UtcNow,
                    Message = htmlEncode ? HttpUtility.HtmlEncode(message) : message,
                    StackTrace = stackTrace,
                    Errors = errors
                },
                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore}
            );

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }

        private static List<ErrorItem> HtmlEncode(List<ErrorItem> errors)
        {
            return errors.Select(err =>
                new ErrorItem
                {
                    Message = HttpUtility.HtmlEncode(err.Message),
                    Type = HttpUtility.HtmlEncode(err.Type)
                }).ToList();
        }
    }
}
