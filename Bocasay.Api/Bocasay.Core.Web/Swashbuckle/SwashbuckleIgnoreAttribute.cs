﻿using System;

namespace KassomPoc.Core.Web.Swashbuckle
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SwashbuckleIgnoreAttribute : Attribute
    {
    }
}
