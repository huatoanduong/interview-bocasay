﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Scrutor;

namespace Bocasay.Core.Web.Extensions
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection WithTransientLifetime<T>(this IServiceCollection services)
        {
            services.Scan(scan => scan
                .FromAssemblyOf<T>()
                .AddClasses()
                .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                .AsSelfWithInterfaces()
                .WithTransientLifetime());

            return services;
        }

        public static IServiceCollection WithGenericTransientLifetime<T>(this IServiceCollection services, Type type)
        {
            services.Scan(scan => scan
                .FromAssemblyOf<T>()
                .AddClasses(classes => classes.AssignableTo(type)).AsMatchingInterface()
                .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                .AsSelfWithInterfaces()
                .WithTransientLifetime());

            return services;
        }

        public static IServiceCollection WithGenericScopedLifetime<T>(this IServiceCollection services, Type type)
        {
            services.Scan(scan => scan
                .FromAssemblyOf<T>()
                .AddClasses(classes => classes.AssignableTo(type)).AsMatchingInterface()
                .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                .AsSelfWithInterfaces()
                .WithScopedLifetime());

            return services;
        }

        public static IServiceCollection WithScopedLifetime<T>(this IServiceCollection services)
        {
            services.Scan(scan => scan
                .FromAssemblyOf<T>()
                .AddClasses()
                .UsingRegistrationStrategy(RegistrationStrategy.Skip)
                .AsMatchingInterface()
                .WithScopedLifetime());

            return services;
        }
    }
}
